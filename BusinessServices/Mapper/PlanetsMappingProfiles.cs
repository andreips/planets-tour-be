﻿using BusinessServices.DTOs;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessServices.Mapper
{
    public class PlanetsMappingProfiles
    {
        public static void AddMappings(MapperProfiles profiles)
        {
            profiles.CreateMap<Planet, PlanetDTO>()
                   .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                   .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                   .ForMember(dest => dest.Avatar, opts => opts.MapFrom(src => src.Avatar))
                   .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                   .ForMember(dest => dest.Status, opts => opts.MapFrom(src => src.Status))
                   .ForMember(dest => dest.Team, opts => opts.MapFrom(src => src.Team))
                   .ReverseMap();
        }
    }
}
