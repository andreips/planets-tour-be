﻿using AutoMapper;

namespace BusinessServices.Mapper
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles()
        {
            CrewsMappingProfiles.AddMappings(this);
            PlanetsMappingProfiles.AddMappings(this);
        }
    }
}
