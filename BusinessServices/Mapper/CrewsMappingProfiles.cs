﻿using BusinessServices.DTOs;
using DataAccess.Models;

namespace BusinessServices.Mapper
{
    public class CrewsMappingProfiles
    {
        public static void AddMappings(MapperProfiles profiles)
        {
            profiles.CreateMap<User, UserDTO>()
                   .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                   .ForMember(dest => dest.Username, opts => opts.MapFrom(src => src.Username))
                   .ForMember(dest => dest.Password, opts => opts.MapFrom(src => src.Password))
                   .ReverseMap();
            profiles.CreateMap<Robot, RobotDTO>()
                   .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                   .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                   .ReverseMap();
            profiles.CreateMap<Team, TeamDTO>()
                   .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                   .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                   .ForMember(dest => dest.CaptainName, opts => opts.MapFrom(src => src.Captain.Name))
                   .ForMember(dest => dest.Robots, opts => opts.MapFrom(src => src.Robots))
                   .ReverseMap();
        }
    }
}
