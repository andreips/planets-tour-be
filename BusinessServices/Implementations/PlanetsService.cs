﻿using AutoMapper;
using BusinessServices.DTOs;
using BusinessServices.Interfaces;
using DataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessServices.Implementations
{
    public class PlanetsService : IPlanetsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PlanetsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public List<PlanetDTO> GetPlanets()
        {
            var planets = _unitOfWork.PlanetsRepository.GetAll(x => x.Team, x => x.Team.Robots, x => x.Team.Captain).ToList();
            return planets.Select(x => _mapper.Map<PlanetDTO>(x)).ToList();
        }
        public PlanetDTO UpdatePlanet(PlanetDTO planetDTO, int captainId)
        {
            var planet = _unitOfWork.PlanetsRepository.Get(x => x.Id == planetDTO.Id, x => x.Team, x => x.Team.Captain);
            if (planet == null) { throw new Exception(); }
            if (planet.Team == null || planet.Team.Captain.Id != captainId)
            {
                var team = _unitOfWork.TeamsRepository.Get(x => x.Captain.Id == captainId, x => x.Captain);
                if (planet != null)
                {
                    planet.Team = team;
                }
            }
            planet.Description = planetDTO.Description;
            planet.Status = planetDTO.Status;
            _unitOfWork.PlanetsRepository.Update(planet);
            _unitOfWork.save();
            return _mapper.Map<PlanetDTO>(planetDTO);
        }
    }
}
