﻿using AutoMapper;
using BusinessServices.DTOs;
using BusinessServices.Interfaces;
using DataAccess.Models;
using DataAccess.UnitOfWork;

namespace BusinessServices.Implementations
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public UserDTO Login(UserDTO userdto)
        {
            var user = _mapper.Map<User>(userdto);
            user.Password = Utils.Utils.GenerateMd5Hash(userdto.Password);
            var retrievedUser = _unitOfWork.UsersRepository.Get(x => x.Username.Equals(user.Username) && x.Password.Equals(user.Password));
            if (retrievedUser != null)
            {
                return _mapper.Map<UserDTO>(retrievedUser);
            }
            return null;
        }
    }
}
