﻿using BusinessServices.DTOs;

namespace BusinessServices.Interfaces
{
    public interface IUsersService
    {
        public UserDTO Login(UserDTO userdto);
    }
}
