﻿using BusinessServices.DTOs;
using System.Collections.Generic;

namespace BusinessServices.Interfaces
{
    public interface IPlanetsService
    {
        List<PlanetDTO> GetPlanets();
        PlanetDTO UpdatePlanet(PlanetDTO planetDTO, int captainId);
    }
}
