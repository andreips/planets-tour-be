﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text;

namespace BusinessServices.Utils
{
    public static class Utils
    {
        public static string GenerateMd5Hash(string password)
        {
            MD5 md5 = MD5.Create();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));
            byte[] result = md5.Hash;
            StringBuilder strBuider = new StringBuilder();

            for (var i = 0; i < result.Length; i++)
            {
                strBuider.Append(result[i].ToString("x2"));
            }

            return strBuider.ToString();
        }

        public static string RandomString(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            return res.ToString();
        }

        public static int GetUserIdByToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var decoded_token = handler.ReadJwtToken(token);
            decoded_token.Payload.SerializeToJson();
            return 1;
        }
    }
}
