﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace planet_tour_be.Authentication
{
    public static class JwtAuthManager
    {
        public const string SecretKey = "JIOBLi6eVjBpvGtWBgJzjWd2QH0sOn5tI8rIFXSHKijXWEt/3J2jFYL79DQ1vKu+EtTYgYkwTluFRDdtF41yAQ==";
        public static string GenerateJWTToken(int id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var header = new JwtHeader(credentials);
            var payload = new JwtPayload
            {
                { "id", id.ToString()},
            };
            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();
            var tokenString = handler.WriteToken(secToken);

            return tokenString;
        }

        public static int GetUserIdByToken(string token)
        {
            var tk = "";
            if (token.Contains("Bearer"))
            {
                tk = token.Split(" ")[1];
            } 
            else
            {
                tk = token;
            }
            var handler = new JwtSecurityTokenHandler();
            var readToken = handler.ReadJwtToken(tk);

            readToken.Payload.TryGetValue("id", out var idObj);

            if (idObj == null)
            {
                throw new Exception("Invalid token");
            }

            try
            {
                var id = int.Parse(idObj.ToString());
                return id;
            }
            catch
            {
                throw new Exception("Invalid token");
            }
        }
    }
}
