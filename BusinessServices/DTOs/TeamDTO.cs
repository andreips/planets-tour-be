﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessServices.DTOs
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CaptainName { get; set; }
        public List<RobotDTO> Robots { get; set; }
    }
}
