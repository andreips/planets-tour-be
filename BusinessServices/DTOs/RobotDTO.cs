﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessServices.DTOs
{
    public class RobotDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
