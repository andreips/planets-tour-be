﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessServices.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
