﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessServices.DTOs
{
    public class PlanetDTO
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public TeamDTO? Team { get; set; }
    }
}
