﻿using BusinessServices.DTOs;
using BusinessServices.Interfaces;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;
using planet_tour_be.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace planet_tour_be.Controllers
{
    [Route("login")]
    public class LoginController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public LoginController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]UserDTO userdto)
        {
            var user = _usersService.Login(userdto);
            if (user != null)
            {
                var token = JwtAuthManager.GenerateJWTToken(user.Id);
                return Ok(new { token });
            }
            return NotFound("Incorrect username or password!");
        }
    }
}
