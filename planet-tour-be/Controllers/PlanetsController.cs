﻿using BusinessServices.DTOs;
using BusinessServices.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using planet_tour_be.Authentication;
using System;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace planet_tour_be.Controllers
{
    [EnableCors("ApiCorsPolicy")]
    /*[Authorize]*/
    [Route("planets")]
    public class PlanetsController : ControllerBase
    {
        private readonly IPlanetsService _planetsService;
        public PlanetsController(IPlanetsService planetsService)
        {
            _planetsService = planetsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPlanets()
        {
            try
            {
                return Ok(_planetsService.GetPlanets());
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("update")]
        [HttpPut]
        public async Task<IActionResult> UpdatePlanet([FromBody] PlanetDTO planetdto)
        {
            var headers = Request.Headers;
            try
            {
                if (headers.ContainsKey("Authorization"))
                {
                    headers.TryGetValue("Authorization", out StringValues auth);
                    var id = JwtAuthManager.GetUserIdByToken(auth.ToString());
                    var planet = _planetsService.UpdatePlanet(planetdto, id);
                    return Ok(planet);
                } 
                else
                {
                    return BadRequest("Invalid token!");
                }
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
