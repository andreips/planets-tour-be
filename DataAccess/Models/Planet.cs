﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace DataAccess.Models
{
    public class Planet
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public Team? Team { get; set; }
    }
}
