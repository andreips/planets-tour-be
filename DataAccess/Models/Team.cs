﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User Captain { get; set; }
        public List<Robot> Robots { get; set; }
    }
}
