﻿using DataAccess.GenericRepository;
using DataAccess.Models;

namespace DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        GenericRepository<Robot> RobotsRepository { get; }
        GenericRepository<User> UsersRepository { get; }
        GenericRepository<Planet> PlanetsRepository { get; }
        GenericRepository<Team> TeamsRepository { get; }
        void save();
    }
}
