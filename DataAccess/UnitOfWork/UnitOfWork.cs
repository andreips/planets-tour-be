﻿using DataAccess.Context;
using DataAccess.GenericRepository;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private AppDbContext _context;
        private GenericRepository<Robot> _robotsRepository;
        private GenericRepository<User> _usersRepository;
        private GenericRepository<Team> _teamsRepository;
        private GenericRepository<Planet> _planetsRepository;

        public UnitOfWork()
        {
            _context = new AppDbContext();
        }

        public GenericRepository<Robot> RobotsRepository
        {
            get
            {
                if (_robotsRepository == null)
                {
                    _robotsRepository = new GenericRepository<Robot>(_context);
                }
                return _robotsRepository;
            }
        }

        public GenericRepository<User> UsersRepository
        {
            get
            {
                if (_usersRepository == null)
                {
                    _usersRepository = new GenericRepository<User>(_context);
                }
                return _usersRepository;
            }
        }

        public GenericRepository<Team> TeamsRepository
        {
            get
            {
                if (_teamsRepository == null)
                {
                    _teamsRepository = new GenericRepository<Team>(_context);
                }
                return _teamsRepository;
            }
        }

        public GenericRepository<Planet> PlanetsRepository
        {
            get
            {
                if (_planetsRepository == null)
                {
                    _planetsRepository = new GenericRepository<Planet>(_context);
                }
                return _planetsRepository;
            }
        }


        public void save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbException e)
            {
                System.IO.File.AppendAllText(@"c:\errors.txt", e.Message);
                throw e;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("Unit of work is being disposed!");
                    _context.Dispose();
                }
                this.disposed = true;

            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
